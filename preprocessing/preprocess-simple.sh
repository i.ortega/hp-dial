#!/usr/bin/env sh

sed -E 's|^--?||;s|^"?–||;s|—||;s|―||;s|^_||;s|^ ||' < "${1:-/dev/stdin}" | 
    uniq | paste - -
