#!/usr/bin/env python3

from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
import logging

exec(open("dialrequirements.py").read())


def start(update, context):
    context.bot.send_message(
        chat_id=update.effective_chat.id, text="Ongi etorri elkarrizketara."
    )


def echo(update, context):
    context.bot.send_message(
        chat_id=update.effective_chat.id, text=update.message.text
    )


def reply(update, context):
    user = update.message.text
    if user == "Agur":
        context.bot.send_message(
            chat_id=update.effective_chat.id, text="Agur"
        )
        updater.stop()
    else:
        sentence = evaluate(" ".join(tokenizer(user)))
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=sentence.strip().capitalize()
        )

updater = Updater(
    token="INSERT YOUR TOKEN HERE", use_context=True
)

dispatcher = updater.dispatcher

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,
)

def main():



    start_handler = CommandHandler("start", start)
    dispatcher.add_handler(start_handler)

    reply_handler = MessageHandler(Filters.text & (~Filters.command), reply)
    dispatcher.add_handler(reply_handler)

    print("Start")
    updater.start_polling()


if __name__ == "__main__":
    main()
