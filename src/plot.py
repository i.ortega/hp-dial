#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import math


def rayleighDistribution(sigma2):
    return lambda x: x / sigma2 * math.exp(-math.pow(x, 2) / (2 * sigma2))


ray = lambda x: rayleighDistribution(20)(x * 5.8 + 0.2) / 0.1356243

# 100 linearly spaced numbers
x = np.linspace(0, 1, 100)
y = list(map(ray, x))

# setting the axes at the centre
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.spines["left"].set_position("zero")
ax.spines["bottom"].set_position("zero")
ax.spines["right"].set_color("none")
ax.spines["top"].set_color("none")
ax.xaxis.set_ticks_position("bottom")
ax.yaxis.set_ticks_position("left")

# plot the function
plt.plot(x, y, "r")

# show the plot
plt.show()
